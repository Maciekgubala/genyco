#pip install biopython

from Bio import SeqIO

fasta_sequences = SeqIO.parse(open("GCA_000001405.29_GRCh38.p14_genomic.fna"),'fasta')
for fasta in fasta_sequences:
    name, sequence = fasta.id, str(fasta.seq)
    print(name)
    # print(sequence)